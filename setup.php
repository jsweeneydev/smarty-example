<?php
// This config file is most likely wrong for you!
// Make sure to change the Smarty directory here!
require_once(__DIR__ . '/../smarty-3.1.33/libs/Smarty.class.php');
$smarty = new Smarty();

class AppSmarty extends Smarty {
  function __construct() {
    // Class Constructor.
    // These automatically get set with each new instance.
    parent::__construct();

    $this->setTemplateDir(__DIR__ . '/templates/');
    $this->setCompileDir(__DIR__ . '/templates_c/');
    $this->setConfigDir(__DIR__ . '/configs/');
    $this->setCacheDir(__DIR__ . '/cache/');
    // Don't do this!
    // $this->caching = Smarty::CACHING_LIFETIME_CURRENT;

    // $this->debugging = true;
  }
}
?>
