# Smarty Example

### About

This is my "Hello World" project using [Smarty](https://www.smarty.net/). It's not much to write home about, but then it's not supposed to be. 😉



### Features

* Importing external files
* Variables
* Config options
* Functions
* Probably some other stuff



### Setup

You should just need to change the setup.php file to point to the correct location:

```php
// This config file is most likely wrong for you!
// Make sure to change the Smarty directory here!
require_once(__DIR__ . '/../smarty-3.1.33/libs/Smarty.class.php');
```

And of course you'll need a machine with Apache + PHP5 set up.



Have fun!