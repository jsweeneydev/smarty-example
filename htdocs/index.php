<?php
// Setup defines the Smarty class that defines
// template, compile, config, and cache dirs
require_once('../setup.php');
$smarty = new AppSmarty();

$smarty->assign('pageTitle', 'Sample Smarty Page');
$smarty->assign('pageDescription', 'Sample Smarty Description');

$data = json_decode(file_get_contents(__DIR__ . '/sampleJson.json'), true);

$smarty->assign('jsonData', $data);
$smarty->display('index.tpl');
?>
