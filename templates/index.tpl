{* First template experiment *}
<!DOCTYPE html>
<html lang="en">
  {include file='head.tpl'}
  <body>
    <header>
      <h1>Smarty Example</h1>
      <p>This is my Smarty template you're viewing on {$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}</p>
    </header>
    <article>
      List of cheeses:
      {strip}
      <ul>
        {foreach from=$jsonData['cheeses'] key=index item=cheese}
        <li>Cheese {$index + 1}: {"$cheese"|lower}</li>
        {/foreach}
      </ul>
      There are {$cheese@total} cheeses!
      {/strip}
    </article>
    <footer>
      <em>About the author</em>
      <p>
        <span>{$jsonData.author.name}</span>
        {assign var=email value=$jsonData.author.email}
        {if $email}
        <a href={"mailto:$email"} />{$email}</a>
        {/if}
      </p>
    </article>
  </body>
</html>
