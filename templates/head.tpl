<head>
  <title>{$pageTitle}</title>
  <meta charset="UTF-8" />
  <meta name="description" content="{$pageDescription}" />
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=yes" />
  <link rel="icon" type="image/x-icon" href="favicon.ico" />
  <style>
    {config_load file='../configs/config.conf'}
    {literal}
      html {
        font-family: courier;
        {/literal}
        color: {#textColor#};
        background-color: {#backgroundColor#};
        {literal}
      }
      article {
        margin: 2em 0;
      }
    {/literal}
  </style>
</head>
